#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import argparse
import numpy as np
np.random.seed(0)
from rouge import Rouge


def evaluate_summary(reference_summary_sents, generated_summary_sents):
    reference_summary = ' '.join([sent for sent in reference_summary_sents])
    generated_summary = ' '.join([sent for sent in generated_summary_sents])
    rouge = Rouge()
    return rouge.get_scores(generated_summary, reference_summary)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Performs ROUGE evaluation on generated and reference summaries', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--reference_summary_sents', required=True, type=list)
    parser.add_argument('--generated_summary_sents', required=True, type=list)
    args = parser.parse_args()
    
    rouge_scores = evaluate_summary(args.reference_summary_sents, args.generated_summary_sents)
