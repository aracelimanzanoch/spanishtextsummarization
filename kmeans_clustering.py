#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import argparse
import numpy as np
np.random.seed(0)
from time import time
from sklearn import metrics
import matplotlib.pyplot as plt
from sklearn.metrics import *
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from pyclustering.cluster.center_initializer import kmeans_plusplus_initializer
from pyclustering.cluster.elbow import elbow
from pyclustering.cluster.silhouette import silhouette_ksearch, silhouette_ksearch_type
from pyclustering.cluster.kmeans import kmeans


def plot_data_pca(title, x, y=None, cmap="viridis", centers=None, n_components=10):
    pca = PCA(n_components=n_components)
    x_pca = pca.fit_transform(x)
    print("Explained variance of each component: ")
    print(pca.explained_variance_)
    fig = plt.figure(figsize=(20, 15))
    plt.scatter(x_pca[:, 0], x_pca[:, 9], c=y, s=50, cmap=plt.cm.get_cmap(cmap, 256))
    if centers is not None:
        centers_pca = pca.transform(centers)
        plt.scatter(centers_pca[:, 0], centers_pca[:, 1], c='black', s=200, alpha=0.5);
    plt.colorbar(ticks=range(256))
    plt.clim(-0.5, 9.5)
    plt.title(title, fontsize=18)
    plt.xlabel('x', fontsize=16)
    plt.ylabel('y', fontsize=16)
    plt.show()
    return x_pca
    
    
def plot_elbow(k_min, k_max, wc_errors, c_line='skyblue', c_value='cadetblue'):
    plt.figure(figsize=(10, 5))
    plt.plot([i for i in range(k_min, k_max)], wc_errors, c=c_line)
    plt.scatter([elbow_model.get_amount()], [wc_errors[[i for i in range(k_min, k_max)].index(elbow_model.get_amount())]], c=c_value)
    plt.ylabel('Intra-cluster error')
    plt.xlabel('Clusters')
    plt.title('K selection through Elbow method')
    plt.show()
    

def benchmark_baseline_model(x, num_clusters):
    for name, model in zip(['K-means'], [kmeans_baseline]):
        print(name + " clusters: ")
        y, centers = model(x, num_clusters)
        plot_data_pca('PCA', x=x, y=y, centers=centers)
        print('name=%-9s\tsilhouette_score=%.3f' % (name, metrics.silhouette_score(x, y, metric='euclidean', sample_size=x.shape[0])))


def kmeans_baseline(x, num_clusters):
    centers = kmeans_plusplus_initializer(x, num_clusters).initialize()
    estimator = kmeans(x, initial_centers=centers)
    estimator.process()
    return estimator.predict(x), estimator.get_centers()
def perform_clustering(feature_matrix, n_clusters):
    x_pca = plot_data_pca('PCA', x)
    return kmeans_baseline(x_pca, n_clusters)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Performs cluster number selection (Elbow method) and clustering through K-Means algorithm', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--feature_matrix', required=True)
    args = parser.parse_args()
    
    # K selection through Elbow method
    elbow_model = elbow(feature_matrix, 2, 83).process()
    print("Estimated number of clusters using Elbow analysis: " + str(elbow_model.get_amount()))
    wc_errors = elbow_model.get_wce()
    plot_elbow(k_min, k_max, wc_errors)
    n_clusters = elbow_model.get_amount()
    
    # K-Means clustering
    perform_clustering(feature_matrix, n_clusters)

