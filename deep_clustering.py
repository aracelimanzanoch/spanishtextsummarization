#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import argparse
from time import time
import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense, Input, Layer, InputSpec
from tensorflow.keras.initializers import VarianceScaling
from tensorflow.keras.optimizers import SGD, Adam
from DEC import DEC


def perform_clustering(x, n_clusters):
    # Params
    init = 'glorot_uniform'
    update_interval = 140
    pretrain_epochs = 300
    n_epochs = 200
    batch_size = 64
    init = VarianceScaling(scale=1. / 3., mode='fan_in', distribution='uniform')
    pretrain_optimizer = Adam(learning_rate=0.001)
    maxiter = 500
    update_interval = 150
    tol = 0.01
    
    # DEC model
    dec = DEC(dims=[x.shape[-1], 500, 500, 2000, 20], n_clusters=n_clusters, init=init)
    # DEC autoencoder pretraining
    dec.pretrain(x=x, optimizer=pretrain_optimizer, epochs=pretrain_epochs, batch_size=batch_size, epochs=n_epochs)
    dec.autoencoder.load_weights('results/ae_weights.h5')
    dec.model.summary()
    
    # DEC clustering
    t0 = time()
    dec.compile(optimizer=SGD(0.001, 0.9), loss='kld')
    y_pred = dec.fit(x, tol=args.tol, maxiter=maxiter, batch_size=batch_size, epochs=epochs, update_interval=update_interval)
    
    print("DEC clusters: \n" + str(y_pred))
    print('Clustering time: ', (time() - t0))
    return y_pred


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Performs clustering through Deep Embedded Clustering model', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--feature_matrix', required=True)
    parser.add_argument('--n_clusters', required=True, type=int)
    parser.add_argument('--save_dir', default='results')
    args = parser.parse_args()
    
    if not os.path.exists(args.save_dir):
        os.makedirs(args.save_dir)
        
    y_pred = perform_clustering(args.feature_matrix, args.n_clusters)

