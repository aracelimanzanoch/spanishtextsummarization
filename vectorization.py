#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import re
import numpy as np
np.random.seed(0)
import pandas as pd
import spacy
from spacy.lang.es import Spanish
nlp = Spanish()
nlp.add_pipe(nlp.create_pipe('sentencizer'))
import torch
from sentence_transformers import SentenceTransformer
from sentence_transformers import models


class Vectorizer:
    def __init__(self, filename, pretrained_model):
        self.sent2vector, self.feature_matrix = self.generate_sentence_vectors(filename, pretrained_model)
        
    def get_vector(self, sentence):
        return self.sent2vector.get(sentence)
    
    def get_sentence(self, vector):
        sentences = [key_value[0] for key_value in self.sent2vector.items() if np.array_equal(key_value[1], vector)]
        if vectors:
            sentence = sentences[0]
        else:
            sentence = None
        return sentence
    
    def get_sentence_vector_dictionary(self):
        return self.sent2vector
    
    def get_feature_matrix(self):
        return self.feature_matrix
    
    @staticmethod
    def load_sentences(filename):
        # Load data from file as string
        data = open(filename, "rb").read().decode("utf-8")
        # Perform minimum normalization on string
        data = re.sub(r'[…—«»\¿\¡]+', '', data).replace("\r\n", "\n")
        sents = [str(s).strip() for s in nlp(data.replace("\r", "").replace("\n", "")).sents]
        # Return sentences
        return sents

    def load_sentence_transformer(self, pretrained_model):
        if pretrained_model == 'beto':
            # Load sentence transformer from BETO cased files
            return SentenceTransformer(modules=[models.BERT("./BETO/cased/pytorch"),
                                                models.Pooling(models.BERT("./BETO/case/pytorch").get_word_embedding_dimension(),
                                                               pooling_mode_mean_tokens=True,
                                                               pooling_mode_cls_token=False,
                                                               pooling_mode_max_tokens=False)])
        elif pretrained_model == 'm-bert':
            # Load sentence transformer from M-BERT
            return SentenceTransformer('distiluse-base-multilingual-cased')
        else:
            print("Invalid value for pretrained model, it can only be set to 'BETO' or 'M-BERT'")

    def generate_sentence_vectors(self, filename, pretrained_model):
        # Load sentences
        sentences = load_sentences(filename)
        # Load sentence-transformer model
        sentence_transformer = self.load_sentence_transformer(pretrained_model=pretrained_model)
        # Generate sentence vectors
        sentence_embeddings = sentence_transformer.encode(sentences)
        # Sentence-vector dictionary dict(zip(sentences, sentence_embeddings))
        sent2vector = dict(zip(sentences, sentence_embeddings))
        # Feature matrix
        feature_matrix = np.vstack(list(sent2vector.values()))
        return sent2vector, feature_matrix

    
if __name__ == "__main__":
    vectorizer = Vectorizer('full_text.txt', 'beto')
    corpus_feature_matrix = vectorizer.get_feature_matrix()
    human_reference_summary = Vectorizer.load_sentences('full_reference.txt')
