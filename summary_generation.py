#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import argparse
import numpy as np
np.random.seed(0)
from hdmedians import medoid
from scipy import spatial


def generate_summary_sentences(y_pred, sent2vector):
    centroids_vectors = []
    centroids_sents = []
    for cluster in set(y_pred):
        x_cluster_i = []
        for idx, vector in enumerate(list(sent2vector.values())):
            if cluster == list(y_pred)[idx]:
                x_cluster_i.append(vector)
        medoid_vector = medoid(np.vstack(x_cluster_i), axis=0)
        centroids_vectors.append(medoid_vector)
        x_cluster_i = [vector for vector in x_cluster_i if set(vector) != set(medoid_vector)]
        tree = spatial.cKDTree(x_cluster_i)
        centroids_vectors.append(x_cluster_i[tree.query(medoid_vector, k=1)[1]])
    for sent in list(sent2vector.keys()):
        for centroid in centroids_vectors:
            if list(sent2vector[sent]) == list(centroid):
                centroids_sents.append(sent)
    return centroid_sents


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generates text summary', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--y_pred', required=True, type=list)
    parser.add_argument('--sent2vector', required=True, type=dict)
    args = parser.parse_args()
    
    summary_sentences = generate_summary_sentences(args.y_pred, args.sent2vector)

